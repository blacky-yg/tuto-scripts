# ANGULAR GETTING STARTED

## SCRIPT

[- Reference](https://docs.microsoft.com/en-us/microsoftteams/platform/get-started/prerequisites?tabs=cli)

- Node Js (v14 LTS release)

- Web Browser

- Visual Studio Code

- https://docs.microsoft.com/en-us/learn/modules/embedded-web-experiences/

- https://github.com/OfficeDev/Microsoft-Teams-Samples

## LINK

[ANGULAR_GETTING_STARTED]()

---

<div align="center">

<a href="https://gitlab.com/blacky-yg" target="_blank"><img src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/gitlab.svg" alt="gitlab.com" width="30"></a>

</div>