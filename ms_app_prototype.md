# Microsoft Teams App Prototype

## SCRIPT

[- Reference](https://docs.microsoft.com/en-gb/microsoftteams/platform/tutorials/get-started-nodejs-app-studio)

- git

- nodjs and npm

- gulp

- Hello Word Template https://github.com/OfficeDev/Microsoft-Teams-Samples.git

- Build and run

- Host with ngrok

- Deploy it on Microsoft Teams

- Download App Studio to update the app package

- Integrate the app to teams by changing the URL

- Same for personal tab

- How to integrate bots to the app

- Register the app in Teams

- Done !!!

## LINK

[MS_APP_PROTOTYPE]()

---

<div align="center">

<a href="https://gitlab.com/blacky-yg" target="_blank"><img src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/gitlab.svg" alt="gitlab.com" width="30"></a>

</div>