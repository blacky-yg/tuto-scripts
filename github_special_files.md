# Special Files

## How to

- Readme (Informations about the project) at the root, .github or docs folder automatically (.md / .markdown)
- License
- Contributing and Contributors
- ChangeLog
- Support
- Code of Conduct
- Codeowners

---

<div align="center">

<a href="https://gitlab.com/blacky-yg" target="_blank"><img src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/gitlab.svg" alt="gitlab.com" width="30"></a>

</div>